/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-double-left': '&#xf100;',
		'icon-double-right': '&#xf101;',
		'icon-file-pdf': '&#xf1c1;',
		'icon-ticket-alt': '&#xf3ff;',
		'icon-check-double': '&#xf560;',
		'icon-file-csv': '&#xf6dd;',
		'icon-remove': '&#xe904;',
		'icon-requests': '&#xe907;',
		'icon-flash': '&#xe906;',
		'icon-dashboard': '&#xe900;',
		'icon-chevron-thin-up': '&#xe901;',
		'icon-stats': '&#xe902;',
		'icon-user': '&#xe971;',
		'icon-user-tie': '&#xe976;',
		'icon-stats-dots': '&#xe99b;',
		'icon-stats-bars1': '&#xe99c;',
		'icon-accessibility': '&#xe9b5;',
		'icon-tree': '&#xe9bc;',
		'icon-warning1': '&#xea07;',
		'icon-info': '&#xea0c;',
		'icon-check-c': '&#xea52;',
		'icon-uncheck-c': '&#xea53;',
		'icon-radio-checked2': '&#xea55;',
		'icon-radio-unchecked': '&#xea56;',
		'icon-phone': '&#xe942;',
		'icon-search': '&#xe986;',
		'icon-zoom-in': '&#xe987;',
		'icon-zoom-out': '&#xe988;',
		'icon-key': '&#xe98d;',
		'icon-wrench': '&#xe991;',
		'icon-file-excel': '&#xeae2;',
		'icon-warning': '&#xea08;',
		'icon-plus': '&#xea0a;',
		'icon-minus': '&#xea0b;',
		'icon-cancel-circle': '&#xea0d;',
		'icon-blocked': '&#xea0e;',
		'icon-cross': '&#xea0f;',
		'icon-checkmark': '&#xea10;',
		'icon-play2': '&#xea15;',
		'icon-pause': '&#xea16;',
		'icon-stop': '&#xea17;',
		'icon-play3': '&#xea1c;',
		'icon-pause2': '&#xea1d;',
		'icon-stop2': '&#xea1e;',
		'icon-spinner11': '&#xe984;',
		'icon-stats-bars': '&#xe99d;',
		'icon-menu': '&#xe9bd;',
		'icon-paragraph-justify': '&#xea7a;',
		'icon-box-add': '&#xe95e;',
		'icon-terminal': '&#xea81;',
		'icon-spinner': '&#xe97a;',
		'icon-cog': '&#xe994;',
		'icon-cogs': '&#xe995;',
		'icon-location': '&#xe947;',
		'icon-file-text2': '&#xe926;',
		'icon-paste': '&#xe92d;',
		'icon-upload3': '&#xe9c8;',
		'icon-exit': '&#xea14;',
		'icon-folder': '&#xe92f;',
		'icon-folder-open': '&#xe930;',
		'icon-files-empty': '&#xe925;',
		'icon-copy': '&#xe92c;',
		'icon-filter': '&#xea5b;',
		'icon-caret-up': '&#xf077;',
		'icon-caret-down': '&#xf078;',
		'icon-check': '&#xf00c;',
		'icon-power-off': '&#xf011;',
		'icon-refresh': '&#xf021;',
		'icon-edit': '&#xf044;',
		'icon-pencil-square-o': '&#xf044;',
		'icon-help': '&#xf059;',
		'icon-eye': '&#xf06e;',
		'icon-exclamation-triangle': '&#xf071;',
		'icon-warning11': '&#xf071;',
		'icon-group': '&#xf0c0;',
		'icon-users': '&#xf0c0;',
		'icon-bars': '&#xf0c9;',
		'icon-navicon': '&#xf0c9;',
		'icon-reorder': '&#xf0c9;',
		'icon-envelope': '&#xf0e0;',
		'icon-dashboard1': '&#xf0e4;',
		'icon-tachometer': '&#xf0e4;',
		'icon-stethoscope': '&#xf0f1;',
		'icon-broken': '&#xf127;',
		'icon-ellipsis-v': '&#xf142;',
		'icon-create': '&#xf196;',
		'icon-building': '&#xf1ad;',
		'icon-toggle-off': '&#xf204;',
		'icon-toggle-on': '&#xf205;',
		'icon-external-link': '&#xe903;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
