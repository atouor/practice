import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'jalali-moment';

const formats: { [key: string]: string } = {
  short: 'jYY/jM/jD h:mm a',
  fullDate: 'jD jMMMM, jEE, jYYYY',
  longDate: 'jD jMMMM, jYYYY',
  mediumDate: 'jD jMM, jYYYY',
  shortDate: 'jYY/jM/jD',
  mediumTime: 'h:mm:ss a',
  shortTime: 'h:mm a'
};

@Pipe({
  name: 'jalali'
})
export class JalaliPipe implements PipeTransform {

  transform(value: any, format: string = 'jYYYY/jMM/jDD HH:mm:ss'): any {
    return moment(new Date(value)).locale('fa').format(format in formats ? formats[format] : format);
  }

}