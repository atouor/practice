import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "splitTitle",
})
export class SplitTitlePipe implements PipeTransform {
  transform(value: string, sign: string = ",", character: string = "/"): any {
    if (!value) {
      return null;
    }
    let newValue = value.split(sign);

    let result = "";
    if (newValue.length > 1) {
      for (let i = 0; i < newValue.length; i++) {
        result += newValue[i] + ` ${character} `;
      }
    }
    result = result.slice(0, -2);
    return result;
  }
}
