import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "splitName",
})
export class SplitNamePipe implements PipeTransform {
  transform(value: string, sign: string = "_", num: number = 0): any {
    if (!value) {
      return null;
    }
    let newValue = value.split(sign);
    let result: string = "";
    if (newValue.length > 1) {
      result = newValue[num];
    } else {
      result = value;
    }
    return result;
  }
}
