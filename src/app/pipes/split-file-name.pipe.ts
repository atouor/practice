import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "splitFileName",
})
export class splitFileName implements PipeTransform {
  transform(value: string, sign: string = ",", character: string = "/"): any {
    if (!value) {
      return null;
    }
    let arrayOfInput = value.split(sign);
    let result = arrayOfInput[arrayOfInput.length - 1];
    return result;
  }
}
