import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

import {UserService} from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAccessGuard implements CanActivate, CanActivateChild {

  constructor(
    private _userService: UserService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._userService.userAccessible(next.data.page, next.data.action);
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._userService.userAccessible(next.data.page, next.data.action);
  }
}
