import { carServices } from './../../../services/car.service';
import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "add-car",
  templateUrl: "./add-car.components.html",
  styleUrls: [],
})
export class addCarCpmponent implements OnInit {
  // get data from parent (our edit id) 
  @Input('parentData') public edit_id;
  car = [];
  constructor(public car_servises : carServices) {
    // getting data from servise 
    this.car = this.car_servises.getdata();
  }
  // our input parametr (data binding)
  name: string;
  year: number;
  price: number;
  value: number;
  flag: number = 0;
  // our edit fuc (puting data into inputs)
  PutData(){
    this.flag = 1;
    this.name= this.car[this.edit_id-1].name;
    this.year=this.car[this.edit_id-1].year;
    this.price= this.car[this.edit_id-1].price;
   this.value=this.car[this.edit_id-1].color;
  }
  // our add button on click (submit type input) from form
  add(){
    // if = watching to we edit or we add new item
    if(this.flag == 0) {
      this.validationInputs();
      if(this.validationInputs()) {
        //there is we add new item
        this.car_servises.setdata(this.name , this.year ,this.price , this.value)
        this.name= "";
        this.year = null;
        this.price= null;
        this.value= null;
      }
    } else{
      // there is we edit item 
      this.validationInputs();
      if(this.validationInputs()) {
        this.flag = 0;
        this.car_servises.editdata(this.name , this.year , this.price , this.value , this.edit_id);
        this.name= "";
        this.year = null;
        this.price= null;
        this.value= null;
      }
    }
  }
  ngOnInit() {}
  //watching our input if it was change tell us and we call  putdata() fuc to edit our data
  ngOnChanges(...args: any[]) {
    this.edit_id  = args[0]['edit_id'].currentValue;
    if(this.edit_id)  {
      this.PutData();
    }
  }
  //validation input = watching we adding completly inputs
  validationInputs() {
    if(this.name == null) return false;
    if(this.year == null) return false;
    if(this.price == null) return false;
    if(this.value == null) return false;
    else return true;
  }
}
