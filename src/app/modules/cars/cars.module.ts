import { carServices } from './../../services/car.service';
import { CarRoutingModuls } from './car-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarComponents } from './cars.components';
import { SharedModule } from '../shared/shared.module';
import { addCarCpmponent } from './add-car/add-car.components';
import { tableCarComponent } from './table-car/table-car.components';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CarRoutingModuls,
  ],
  declarations: [
    CarComponents,
    addCarCpmponent,
    tableCarComponent,
  ]
})
export class Carmoduls {
}
