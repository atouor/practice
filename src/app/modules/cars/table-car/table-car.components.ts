import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { carServices } from './../../../services/car.service';
;

@Component({
  selector: "car-table",
  templateUrl: "./table-car.components.html",
  styleUrls: [],
})
export class tableCarComponent implements OnInit {
  // my modal value
  cars =[];
  NAME :string;
  COLOR : number;
  PRICE : number;
  YEAR :number;
  constructor(private car : carServices) {
  // getting data for table from service
    this.cars=this.car.getdata();
  }
  // getting view from html to change display
  @ViewChild('myview', {static: false}) my_dialog: ElementRef;
  // sending edit id from child to parent component
  @Output() public carid = new EventEmitter();
  view_item(ID:number){
    this.NAME=this.cars[ID-1].name;
    this.COLOR=this.cars[ID-1].color;
    this.PRICE=this.cars[ID-1].price;
    this.YEAR=this.cars[ID-1].year;
    this.my_dialog.nativeElement.style.display = 'block';
  }
  CloseModal(){
    this.my_dialog.nativeElement.style.display = 'none';
  }
  // call delete data from service
  delete_item(ID){
    if(ID) {
      this.car.deletedata(ID);
    }
  }
  // send edit id (emite)
  edit_item(ID){this.carid.emit(ID);}

  ngOnInit() {}
}