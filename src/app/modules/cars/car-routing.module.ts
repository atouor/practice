import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CarComponents } from './cars.components';


const routes: Routes = [
  {
    path: '',
    component: CarComponents,
    data: {
      title: 'car'
    }
  },
  {
    path: "",
    redirectTo: "/car",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/car",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarRoutingModuls {
}
