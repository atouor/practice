import { Component, OnInit } from "@angular/core";

@Component({
  selector: "cars-component",
  templateUrl: "./cars.components.html",
  styleUrls: [],
})
export class CarComponents implements OnInit {
  // resiving our id for edit from child
  myidnumber_edit : number;
  constructor() {}
  ngOnInit() {}
  setCarId(event) {
    this.myidnumber_edit = event; 
  }
}