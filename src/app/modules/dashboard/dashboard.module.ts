import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { NumberPipePipe } from './../../pipes/number-pipe.pipe';
import { DashboardComponent } from './mainDashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ],
  declarations: [
    DashboardComponent, NumberPipePipe,
  ]
})
export class DashboardModule {
}
