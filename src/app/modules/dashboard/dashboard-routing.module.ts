import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { DashboardComponent } from './mainDashboard/dashboard.component';


const routes: Routes = [
  {
    path: 'main',
    component: DashboardComponent,
    data: {
      title: 'dashboard.title'
    }
  },
  {
    path: "",
    redirectTo: "/dashboard/main",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/dashboard/main",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
