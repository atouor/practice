import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from "@angular/core";

interface Item {
  name: string;
  id: string;
  checked: boolean;
}

@Component({
  selector: "select-input-header",
  templateUrl: "./header-select-input.component.html",
  styles: [],
})
export class HeaderSelectInputComponent implements OnInit {
  @Input() public list: any[] = [];
  @Input() defaults: string = "";
  @Input() searchable: boolean = false;
  @Input() multi: boolean = false;

  @Output() public changed: EventEmitter<object[]> = new EventEmitter<
    object[]
  >();
  public inputFocused: boolean = false;
  @ViewChild("toggleHeader", { static: false })
  toggleHeader: ElementRef;
  @ViewChild("menu", { static: false }) menu: ElementRef;
  public showList: boolean = false;
  public unselectedList: Item[] = [];
  public selectedList: Item[] = [];
  public inputValue: string = "";
  checked: boolean = false;
  public pattern: RegExp = / +/g;

  constructor(private renderer: Renderer2) {
    this.unselectedList = [...this.list];
    this.unselectedList.forEach((item) => {
      item["checked"] = false;
    });
  }

  public onClickHeader(event: Event): void {
    if (this.newFilteredList.length) {
      this.unselectedList = [...this.newFilteredList];
    } else {
      this.unselectedList = [...this.list];
    }

    this.inputFocused = !this.inputFocused;
  }

  public clearAllChecked(event: Event) {
    this.unselectedList = [...this.list];
    this.unselectedList = this.unselectedList.map((item) => {
      item.checked = false;
      return item;
    });
    this.selectedList = [];
    this.newFilteredList = [];
    this.inputValue = "";
    this.changed.emit([]);
  }

  ngOnInit() {}

  public newFilteredList = [];
  public checkItem(selectedItem: Item, isChecked: boolean): void {
    if (this.multi) {
      if (isChecked) {
        if (!this.selectedList.includes(selectedItem)) {
          this.selectedList.push(selectedItem);
          this.changed.emit(this.selectedList);
        }
        setTimeout(() => {
          if (this.unselectedList.includes(selectedItem)) {
            this.unselectedList = this.unselectedList.filter(
              (item) => item.name !== selectedItem.name
            );
          }
        }, 200);
      } else {
        if (this.selectedList.includes(selectedItem)) {
          setTimeout(() => {
            this.selectedList = this.selectedList.filter(
              (item) => item.name !== selectedItem.name
            );
            this.changed.emit(this.selectedList);
          }, 200);
        }
        if (!this.unselectedList.includes(selectedItem)) {
          setTimeout(() => {
            this.unselectedList.unshift(selectedItem);
          }, 200);
        }
      }
    } else {
      selectedItem.checked = isChecked;
      this.unselectedList = this.unselectedList.map((item) => {
        if (item.name === selectedItem.name) {
          item.checked = selectedItem.checked;
        } else {
          item.checked = false;
        }
        return item;
      });
      this.inputFocused = !this.inputFocused;
      if (selectedItem.checked) {
        this.changed.emit([selectedItem]);
      } else {
        this.changed.emit([]);
      }
    }
  }

  public searchTerm(event: Event): void {
    this._filterList();
  }

  public onClickInput(event: Event): void {}

  public blured(event: Event): void {
    this.renderer.listen("window", "click", (e: Event) => {
      if (this.inputFocused) {
        if (
          !this.menu.nativeElement.contains(e.target) &&
          !this.toggleHeader.nativeElement.contains(e.target)
        ) {
          if (this.multi) {
            let difrence = this.list.filter(
              (item) => !this.selectedList.includes(item)
            );
            this.newFilteredList = [...difrence];
            if (!this.newFilteredList.length) {
              this.selectedList = [];
            }
          }

          setTimeout(() => {
            this.inputFocused = false;
            this.inputValue = "";
          }, 200);
        }
      }
    });
  }

  private _filterList(): void {
    if (this.list == null) return;
    let listClone = [...this.list];
    if (this.selectedList && this.selectedList.length) {
      listClone = this.list.filter((item) => !this.selectedList.includes(item));
    }
    this.unselectedList = listClone.filter((item: Item) => {
      let text = item.name.toLowerCase(),
        phrase = this.inputValue.toLowerCase();
      return text.includes(phrase);
    });
  }
}
