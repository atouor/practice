import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NgxSmartModalService } from "ngx-smart-modal";
import * as moment from "moment-jalali";
import * as momentOriginal from "moment";
import { QueryService } from "../../../services/query.service";
import { ApiService } from "../../../services/api.service";
import { LocalStorageService } from "angular-2-local-storage";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

declare let Highcharts: any;
declare let $: any;

@Component({
  selector: "gauge-chart",
  templateUrl: "./gauge-chart.component.html",
  styles: [],
})
export class GaugeChartComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild("chart", { static: true }) chart: ElementRef;

  @Input("data") data;
  @Input("kpi_id") kpi_id: string = "";
  @Input("sensor_id") sensor_id: string = "";
  @Input("another_kpi_id") another_kpi_id: string = "";
  @Input("node_id") node_id: string = "";
  @Input("desc") desc: string = "";
  @Input("uniqueID") uniqueID: string = "";
  @Input("showButtons") showButtons: boolean = false;
  @Input("url") url: string = "";
  @Input("isOffline") isOffline: string = "";
  @Input("min") min: number = 0;
  @Input("isSmaller") isSmaller: boolean = false;
  @Input("seriesName") seriesName: boolean = false;
  @Input("fourColumn") fourColumn: boolean = false;
  @Input("node_enabled") node_enabled: boolean = true;

  public identifier = this.uniqueID + "_custom-time";
  public gaugeOptions = {};
  public chartGauge;
  public lang = this._storage.get("lang");
  public defaultStart = this._query.convertUnix(moment());
  public defaultEnd = this._query.convertUnix(moment());
  public startDate = "";
  public endDate = "";
  public start: number;
  public end: number;
  public date = new Date();
  public checked = true;

  public label = "";
  private destroy$ = new Subject();
  constructor(
    private _translate: TranslateService,
    public modal: NgxSmartModalService,
    private _query: QueryService,
    private _api: ApiService,
    private _storage: LocalStorageService
  ) {}

  ngOnChanges() {}

  ngOnInit() {
    switch (this.uniqueID) {
      case "average-total-availability-per-month":
      case "percentage-connected-branches":
      case "packet-loss":
      case "cpu-usage":
      case "ram-free":
        this.label = " %";
        break;
      case "jitter":
      case "ping":
        this.label = " ms";
        break;
      case "temperutre":
        this.label = " &deg;C";
        break;

      default:
        break;
    }

    this.gaugeOptions = {
      chart: {
        type: "solidgauge",
      },

      title: null,

      pane: {
        center: ["50%", "85%"],
        size: this.isSmaller
          ? "100%"
          : this.uniqueID === "cpu-usage" ||
            this.uniqueID === "ram-free" ||
            this.uniqueID === "temperutre"
          ? "115%"
          : this.uniqueID === "ticket1" ||
            this.uniqueID === "ticket2" ||
            this.uniqueID === "ticket3" ||
            this.uniqueID === "ticket4" ||
            this.uniqueID === "ticket5" ||
            this.uniqueID === "ticket6" ||
            this.uniqueID === "ticket7" ||
            this.uniqueID === "ticket8" ||
            this.uniqueID === "ticket9"
          ? "110%"
          : "140%",
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || "#EEE",
          innerRadius: "90%",
          outerRadius: "100%",
          shape: "arc",
        },
      },

      exporting: {
        enabled: false,
      },

      tooltip: {
        enabled: false,
      },

      // the value axis
      yAxis: {
        stops: this.colorStop(),
        lineWidth: 0,
        tickWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
      },

      plotOptions: {
        solidgauge: {
          innerRadius: "90%",
          dataLabels: {
            y: 5,
            borderWidth: 0,
            useHTML: true,
          },
        },
      },
    };
    this.chart.nativeElement.id = this.uniqueID;

    if (this.data != undefined) {
      this._translate.onLangChange
        .pipe(takeUntil(this.destroy$))
        .subscribe((res) => {
          if (res["lang"] === "fa") {
            this.lang = "fa";
            this.desc = this._translate.instant(this.desc);

            this._initChart(this.data, this.uniqueID, this.desc, this.label);
          } else {
            this.lang = "en";
            this.desc = this._translate.instant(this.desc);
            this._initChart(this.data, this.uniqueID, this.desc, this.label);
          }
        });
    } else {
      if (this.kpi_id != "" && this.node_id != "")
        this._selectTime(
          this.uniqueID + "_select-last-hour",
          this.uniqueID,
          "second"
        );
    }
    this._translate.onLangChange.subscribe((res) => {
      if (res["lang"] === "fa") {
        this.lang = "fa";
      } else {
        this.lang = "en";
      }
    });
    if (this.lang === "fa") {
      if (this.data != undefined) {
        this._translate.get(this.desc, {}).subscribe((text) => {
          this._initChart(this.data, this.uniqueID, text, this.label);
        });
      } else {
        if (this.kpi_id != "" && this.node_id != "")
          this._selectTime(
            this.uniqueID + "_select-last-hour",
            this.uniqueID,
            "second"
          );
      }
    } else {
      if (this.data != undefined) {
        this._translate.get(this.desc, {}).subscribe((text) => {
          this._initChart(this.data, this.uniqueID, text, this.label);
        });
      } else {
        if (this.kpi_id != "" && this.node_id != "")
          this._selectTime(
            this.uniqueID + "_select-last-hour",
            this.uniqueID,
            "second"
          );
      }
    }
  }

  public setTimeSearch(date, formC): void {
    if (formC == "startDate") {
      this.start = date;
    } else {
      this.end = date;
    }
  }

  public _selectTime(t, idChart?: string, period?: string): void {
    switch (period) {
      case "second": {
        if (
          this.uniqueID == "ping" ||
          this.uniqueID == "jitter" ||
          this.uniqueID == "packet-loss"
        ) {
          if (this.node_enabled && this.sensor_id) {
            this._api.set(
              `monitoring/sensor/${this.sensor_id}/last`,
              "GET",
              {},
              (res) => {
                let label = " ms";
                let data = res.data[1];
                if (this.uniqueID === "ping" || this.uniqueID === "jitter") {
                  data = data * 1000;
                }
                data = data.toFixed(1);
                data = Number(data);
                if (this.uniqueID == "packet-loss") {
                  label = " %";
                }
                this._translate.get(this.desc, {}).subscribe((text) => {
                  this._initChart(data, this.uniqueID, text ? text : "", label);
                });
              }
            );
          } else {
            this._translate.get(this.desc, {}).subscribe((text) => {
              this._initChart(0, this.uniqueID, text ? text : "", "");
            });
          }
        } else if (
          this.uniqueID === "cpu-usage" ||
          this.uniqueID === "ram-free" ||
          this.uniqueID === "temperutre"
        ) {
          this._api.set(
            `node/${this.node_id}/service/resource_utilization/snmp`,
            "GET",
            {
            },
            (res) => {
              let ramCapacity = 0;
              let ramFree = 0;
              let ramUsage;
              let ramTotolUsage = 0;
              let cpuUsage = 0;
              let cpuCount = 0;
              let temperutre = 0;
              let tempNum = 0;
              let tempTotal = 0;
              let ramTotal = 0;
              res.resource_utilization.forEach((resource) => {
                if (resource.sensor.name.includes("ramFree")) {
                  ramCapacity += resource.last_value;
                  ramFree += resource.last_value;
                }
                if (resource.sensor.name.includes("ramUsage")) {
                  ramTotolUsage += resource.last_value;

                  ramCapacity += resource.last_value;
                }
                if (resource.sensor.name.includes("ramTotal")) {
                  ramTotal += resource.last_value;
                }
                if (resource.sensor.name.includes("cpuUsage")) {
                  cpuUsage += resource.last_value;
                  cpuCount++;
                }
                if (resource.sensor.name.includes("temperature")) {
                  tempTotal += resource.last_value;
                  tempNum++;
                }
              });

              if (this.uniqueID === "cpu-usage") {
                let result = 0;
                if (cpuCount !== 0) {
                  result = Number((cpuUsage / cpuCount).toFixed(2));
                }
                this._initChart(result, this.uniqueID, "CPU", "%");
              }
              if (this.uniqueID === "temperutre") {
                if (tempNum && tempTotal) {
                  temperutre = Number((tempTotal / tempNum).toFixed(2));
                }
                this._initChart(
                  temperutre,
                  this.uniqueID,
                  "Temperature",
                  " &deg;C"
                );
              }
              if (this.uniqueID === "ram-free") {
                debugger;
                if (ramTotal && ramTotolUsage) {
                  let usage = 0;
                  usage = Number(((ramTotolUsage / ramTotal) * 100).toFixed(2));
                  this._initChart(usage, this.uniqueID, "RAM", "%");
                } else {
                  let usage = ramCapacity - ramFree;
                  ramUsage = 0;
                  if (usage !== 0 && ramCapacity !== 0) {
                    ramUsage = ((usage / ramCapacity) * 100).toFixed(2);
                    ramUsage = Number(ramUsage);
                  }
                  this._initChart(ramUsage, this.uniqueID, "RAM", "%");
                }
              }
            }
          );
        }
        break;
      }
      case "daily": {
        const startDate = Math.floor(
          this._query.convertUnix(moment().startOf("day")) / 1000
        );
        const endDate = Math.floor(this._query.convertUnix(moment()) / 1000);
        switch (this.uniqueID) {
          case "jitter":
            this.drawJitterChart("daily", startDate, endDate);
            break;
          case "ping":
            this.drawPingChart("daily", startDate, endDate);
            break;
          case "packet-loss":
            this.drawPacketLostChart("daily", startDate, endDate);
            break;
          case "cpu-usage":
            this.drawCpuChart("daily", startDate, endDate);
            break;
          case "temperutre":
            this.drawTemp("daily", startDate, endDate);
            break;
          case "ram-free":
            this.drawRamChart("daily", startDate, endDate);
            break;

          default:
            this.defaultDrawChart("daily", startDate, endDate);
            break;
        }
        break;
      }
      case "monthly": {
        const startDate = Math.floor(
          this._query.convertUnix(moment().subtract(30, "days")) / 1000
        );
        const endDate = Math.floor(this._query.convertUnix(moment()) / 1000);
        switch (this.uniqueID) {
          case "jitter":
            this.drawJitterChart("monthly", startDate, endDate);
            break;
          case "ping":
            this.drawPingChart("monthly", startDate, endDate);
            break;
          case "packet-loss":
            this.drawPacketLostChart("monthly", startDate, endDate);
            break;
          case "cpu-usage":
            this.drawCpuChart("monthly", startDate, endDate);
            break;
          case "temperutre":
            this.drawTemp("daily", startDate, endDate);
            break;
          case "ram-free":
            this.drawRamChart("monthly", startDate, endDate);
            break;

          default:
            this.defaultDrawChart("monthly", startDate, endDate);
            break;
        }
        break;
      }
      case "custom": {
        let startTime = momentOriginal(this.start);
        let endTime = momentOriginal(this.end);
        let dif = endTime.diff(startTime);
        let du = momentOriginal.duration(dif);
        let days = du.asDays();
        let start = this._query.convertUnix(this.start) / 1000;
        let end = this._query.convertUnix(this.end) / 1000;
        let period = "";
        if (days <= 1) {
          period = "daily";
        } else if (days > 1 && days < 30) {
          period = "weekly";
        } else if (days >= 30) {
          period = "monthly";
        }
        switch (this.uniqueID) {
          case "jitter":
            this.drawJitterChart(period, start, end);
            break;
          case "ping":
            this.drawPingChart(period, start, end);
            break;
          case "packet-loss":
            this.drawPacketLostChart(period, start, end);
            break;
          case "cpu-usage":
            this.drawCpuChart(period, start, end);
            break;
          case "temperutre":
            this.drawTemp("daily", start, end);
            break;
          case "ram-free":
            this.drawRamChart(period, start, end);
            break;

          default:
            this.defaultDrawChart(period, start, end);
            break;
        }

        break;
      }
    }
  }
  public drawJitterChart(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {  },
        (res) => {
          let data = res.accessibility[0].avg_value || 0;
          let label = " ms";
          if (data < 1) {
            data = data.toString();
            if (data.includes("e")) {
              let e = data.indexOf("e");
              data = data.slice(0, e);
              data = parseFloat(data);
              data = data * 1000;
              data = data.toFixed(1);
              data = Number(data);
            }
            data = parseFloat(data);
            data = data * 1000;
            data = data.toFixed(1);
            data = Number(data);
          }
          this._translate.get(this.desc, {}).subscribe((text) => {
            this._initChart(data, this.uniqueID, text ? text : "", label);
          });
        }
      );
    }
  }
  public drawPingChart(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        { },
        (res) => {
          let data = res.accessibility[0].avg_value || 0;
          let label = " ms";
          if (data < 1) {
            data = data.toString();
            if (data.includes("e")) {
              let e = data.indexOf("e");
              data = data.slice(0, e);
              data = parseFloat(data);
              data = data * 1000;
              data = data.toFixed(1);
              data = Number(data);
            }
            data = parseFloat(data);
            data = data * 1000;
            data = data.toFixed(1);
            data = Number(data);
          }
          this._translate.get(this.desc, {}).subscribe((text) => {
            this._initChart(data, this.uniqueID, text ? text : "", label);
          });
        }
      );
    }
  }
  public drawPacketLostChart(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {  },
        (res) => {
          let data = res.accessibility[0].avg_value || 0;
          let label = " %";
          data = data.toFixed(1);
          data = Number(data);
          this._translate.get(this.desc, {}).subscribe((text) => {
            this._initChart(data, this.uniqueID, text ? text : "", label);
          });
        }
      );
    }
  }
  public drawCpuChart(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {},
        (res) => {
          let cpuUsage = res.accessibility[0].avg_value;
          cpuUsage = cpuUsage.toFixed(2);
          cpuUsage = Number(cpuUsage);
          this._initChart(cpuUsage, this.uniqueID, "CPU", "%");
        }
      );
    }
  }
  public drawTemp(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {},
        (res) => {
          let temprature = res.accessibility[0].avg_value;
          temprature = temprature.toFixed(2);
          temprature = Number(temprature);
          this._initChart(temprature, this.uniqueID, "Temperature", " &deg;C");
        }
      );
    }
  }
  public drawRamChart(period: string, start: number, end: number): void {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {},
        (resUsage) => {
          let ramUsage = resUsage.accessibility[0].avg_value;
          let ramFree = 0;
          let ramCapacity = 0;
          let result;
          this._api.set(
            `node/${this.node_id}/avrage/${this.another_kpi_id}?period=${period}&start=${start}&end=${end}`,
            "GET",
            {},
            (resFree) => {
              ramFree = resFree.accessibility[0].avg_value;
              ramCapacity = ramFree + ramUsage;
              if (ramUsage !== 0 && ramCapacity !== 0) {
                result = ((ramUsage / ramCapacity) * 100).toFixed(2);
                result = Number(result);
                this._initChart(result, this.uniqueID, "RAM", "%");
              } else {
                this._initChart(0, this.uniqueID, "RAM", "%");
              }
            }
          );
        }
      );
    }
  }
  public defaultDrawChart(period: string, start: number, end: number) {
    if (this.kpi_id) {
      this._api.set(
        `node/${this.node_id}/avrage/${this.kpi_id}?period=${period}&start=${start}&end=${end}`,
        "GET",
        {  },
        (res) => {
          let data = res.accessibility[0].avg_value || 0;
          let label = " %";
          if (data < 1 && this.uniqueID == "jitter") {
            data = data.toString();
            if (data.includes("e")) {
              let e = data.indexOf("e");
              data = data.slice(0, e);
              data = parseFloat(data);
              data = data * 1000;
              data = data.toFixed(1);
              data = Number(data);
            }
            data = parseFloat(data);
            data = data * 1000;
            data = data.toFixed(1);
            data = Number(data);
          }
          if (this.uniqueID != "jitter") {
            data = data.toFixed(1);
            data = Number(data);
          }
          if (this.uniqueID == "ping" || this.uniqueID == "jitter") {
            label = " ms";
          }
          this._translate.get(this.desc, {}).subscribe((text) => {
            this._initChart(data, this.uniqueID, text ? text : "", label);
          });
        }
      );
    }
  }
  public colorStop(): any {
    let colorStop = [
      [0.1, "#0FAF4B"], // green
      [0.5, "#F9C08C"], // yellow
      [0.9, "#DF5353"], // red
    ];
    switch (this.uniqueID) {
      case "average-total-availability-per-month": {
        colorStop = [
          [0.1, "#DF5353"], // red
          [0.5, "#F9C08C"], // yellow
          [0.9, "#0FAF4B"], // green
        ];
        return colorStop;
      }
      case "percentage-connected-branches": {
        colorStop = [
          [0.1, "#DF5353"], // red
          [0.5, "#F9C08C"], // yellow
          [0.9, "#0FAF4B"], // green
        ];
        return colorStop;
      }
      case "RSRP":
      case "SIGNALQUALITY":
      case "RSRP1":
      case "RSSI":
      case "SINR":
      case "RSRP0":
      case "RSRQ": {
        colorStop = [
          [0.25, "#DF5353"], // red
          [0.45, "#F9C08C"], // yellow
          [0.55, "#F9C08C"], // green
          [0.85, "#00ba60"], // dark green
        ];
        return colorStop;
      }
    }
    return colorStop;
  }

  public maxById(id: string) {
    switch (id) {
      case "ping":
        return 1000;
      case "jitter":
        return 500;
      case "SINR":
        return 22;
      case "RSRQ":
        return 0;
      case "RSRP":
      case "RSRP1":
      case "RSRP0":
        return -50;
      case "RSSI":
        return -60;
      default:
        return 100;
        break;
    }
  }
  public minById(id: string) {
    switch (id) {
      case "ping":
        return 0;
      case "jitter":
        return 0;
      case "SINR":
        return -6;
      case "RSRQ":
        return -20;
      case "RSRP":
      case "RSRP1":
      case "RSRP0":
        return -175;
      case "RSSI":
        return -100;
      default:
        return 0;
        break;
    }
  }
  closeModal(){
    this.modal.get(`${this.uniqueID}_custom-time`).close()
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this._api.remove("get:monitoringSensor");
    this._api.remove("get:snmpData");
    this._api.remove("get:averageStatistic");
  }

  private _initChart(
    data: number,
    id,
    desc?,
    label?: string,
    min: number = this.min
  ) {
    if (typeof data == "string") {
      data = Number(data);
    }
    if (typeof min == "string") {
      min = Number(min);
    }
    const signals = [
      "RSRQ",
      "RSRP0",
      "RSRP1",
      "RSRP",
      "SIGNALQUALITY",
      "SINR",
      "RSSI",
    ];

    this.chartGauge = Highcharts.chart(
      id,
      Highcharts.merge(this.gaugeOptions, {
        // the value axis
        yAxis: {
          min: this.minById(id),
          max: this.maxById(id),
          stops: this.colorStop(),
          lineWidth: 0,
          tickWidth: 0,
          minorTickInterval: null,
          tickAmount: 2,
          title: {
            style: {
              fontsize: "2rem",
            },
            text: this.seriesName ? this.uniqueID : "",
            y: this.isSmaller ? -60 : -70,
          },
          labels: {
            y: 16,
          },
        },

        credits: {
          enabled: false,
        },
        series: [
          {
            data: [data],
            dataLabels: {
              format: this.isSmaller
                ? '<div style="text-align:center;padding-top:3.5rem">' +
                  '<span style="font-size:1.7rem">{y}' +
                  label +
                  "</span><hr/>" +
                  '<span style="font-size:1rem;opacity:0.5">' +
                  desc +
                  "</span>" +
                  "</div>"
                : this.fourColumn
                ? '<div style="text-align:center;padding-top:2.5rem">' +
                  '<span style="font-size:2rem">{y}' +
                  label +
                  "</span><hr/>" +
                  '<span style="font-size:1rem;opacity:0.5">' +
                  desc +
                  "</span>" +
                  "</div>"
                : !this.node_enabled
                ? '<div style="text-align:center">' +
                  '<span style="font-size:2rem">' +
                  "N/A" +
                  "</span><br/>" +
                  '<span style="font-size:8px;opacity:0.4">' +
                  desc +
                  "</span>" +
                  "</div>"
                : data < 0
                ? '<div style="text-align:center">' +
                  '<span style="font-size:2rem">' +
                  "N/A" +
                  "</span><br/>" +
                  '<span style="font-size:8px;opacity:0.4">' +
                  desc +
                  "</span>" +
                  "</div>"
                : '<div style="text-align:center">' +
                  '<span style="font-size:2rem">{y}' +
                  label +
                  "</span><br/>" +
                  '<span style="font-size:8px;opacity:0.4">' +
                  desc +
                  "</span>" +
                  "</div>",
            },
            tooltip: {
              valueSuffix:
                signals.indexOf(id) > -1 ? " " : id == "ping" ? " ms" : " %",
            },
          },
        ],
      })
    );
  }
}
