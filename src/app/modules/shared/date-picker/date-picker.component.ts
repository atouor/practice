import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import * as moment from "moment";
import { TranslateService } from "@ngx-translate/core";
import { Subject } from "rxjs";

declare var $: any;
declare var pDatepicker: any;

@Component({
  selector: "date-picker",
  templateUrl: "./date-picker.component.html",
  styles: [],
})
export class DatePickerComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild("datePicker", { static: true }) datePicker: ElementRef;
  @ViewChild("altdatePicker", { static: true }) altdatePicker: ElementRef;
  @Output() date: EventEmitter<any> = new EventEmitter();
  @Input("uniqID") uniqID: string = "";
  @Input("default") default: string = "";
  @Input("showTime") showTime: string = "true";

  public lang = this._storage.get("lang");
  public init: boolean = false;
  private _dp;
  onChanges = new Subject<SimpleChanges>();
  constructor(
    private _storage: LocalStorageService,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this._translate.onLangChange.subscribe((res) => {
      if (res["lang"] === "fa") {
        this.init = false;
        this.lang = "fa";
        this.initDatePicker();
      } else {
        this.init = false;
        this.lang = "en";
        this.initDatePicker();
      }
    });

    this.initDatePicker();
    this.onChanges.subscribe((data: SimpleChanges) => {
      if (this.default != "") {
        this._dp.setDate(parseInt(this.default));
      }
    });
    if (this.default != "") {
      this._dp.setDate(parseInt(this.default));
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.onChanges.next(changes);
  }

  public initDatePicker(): void {
    if (!this.init) {
      this._dp = $(this.datePicker.nativeElement).pDatepicker({
        initialValue: this.default == "" ? false : true,
        calendarType: this.lang == "en" ? "gregorian" : "persian",
        observer: true,
        format: this.showTime == "true" ? "HH:mm:ss YYYY/MM/DD" : "YYYY/MM/DD",
        altFieldFormatter: (unix) => {
          this.emitDate(unix);
          return unix;
        },
        altField: "." + this.uniqID.toString(),
        persian: {
          locale: "fa",
          showHint: true,
        },
        gregorian: {
          locale: "en",
          showHint: true,
        },
        timePicker: {
          enabled: this.showTime == "true" ? true : false,
          step: 1,
          hour: {
            enabled: true,
            step: null,
          },
          minute: {
            enabled: true,
            step: null,
          },
          second: {
            enabled: true,
            step: null,
          },
        },
      });
      this.init = true;
    }
  }

  public emitDate(timeS): void {
    this.date.emit(moment(timeS).toISOString());
  }
  ngOnDestroy() {
    this.onChanges.complete();
  }
}
