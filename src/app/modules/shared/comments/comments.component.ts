import { Component, OnInit, Input } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { ApiService } from "../../../services/api.service";
import { QueryService } from "../../../services/query.service";
import Filters = ApiModel.Filters;

@Component({
  selector: "comments",
  templateUrl: "./comments.component.html",
  styles: [],
})
export class CommentsComponent implements OnInit {
  public filters: Filters = {
    limit: 10,
    q: "",
    page: 1,
    sort: "",
    ...this.query.params(),
  };
  public comments: any = [];
  public actionList = [
    {
      name: "disabling",
      id: 1,
    },
    {
      name: "enabling",
      id: 2,
    },
    {
      name: "IMEI_CHANGED",
      id: 3,
    },
    {
      name: "IMSI_CHANGED",
      id: 4,
    },
  ];
  public totalPages: number = 1;
  public totalCount: number = 1;
  public perPage = 10;
  @Input() public entityID: string = "";

  constructor(
    public translate: TranslateService,
    public query: QueryService,
    private _api: ApiService
  ) {}

  ngOnInit() {
    this.getComments(1);
  }

  public getAction(e) {
    if (e.length) {
      this.filters["action"] = e[0].name;
      this.getComments(1);
    } else {
      this.filters["action"] = "";
      this.getComments(1);
    }
  }

  public getEntity(e) {
    if (e.length) {
      this.entityID = e[0].id;
      this.getComments(1);
    } else {
      this.filters["entity_id"] = "";
      this.getComments(1);
    }
  }

  public getGroupPerPage(event): void {
    this.filters.limit = event;
    this.perPage = event;
    this.getComments(1);
  }

  public getComments(pageNumber: number = this.filters.page): void {
    this.filters.page = pageNumber;
    this.filters["entity_id"] = this.entityID;
    this._api.set(
      "comment",
      "GET",
      { id: "get:comment", params: this.filters },
      (res: any) => {
        this.comments = res.comments || [];
        this.filters.page = res.page == 0 ? 1 : res.page;
        this.totalPages = Math.ceil(res.total_count / this.filters.limit);
        this.totalCount = res.total_count;
      }
    );
  }

  ngOnDestroy() {
    this._api.remove("get:comment");
  }
}
