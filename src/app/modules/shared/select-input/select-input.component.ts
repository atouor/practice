import {
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";

interface Item {
  name: string;
  id: string;
}

@Component({
  selector: "select-input",
  templateUrl: "./select-input.component.html",
  styles: [],
  host: {
    "[class.is-empty]": "!this.items.length",
  },
})
export class SelectInputComponent implements OnInit {
  @Input() public placeholder: string = "";
  @Input() public inputid: string = "select-input";
  @Input() public disabled: boolean = false;
  @Input() public list: any[] = [];
  @Input() public translate: string = undefined;
  @Input() public itemkey: string = "name";
  @Input() public itemvalue: string = "id";
  @Input() public multi: boolean = false;
  @Input() public maxLength: number = 1000;
  @Input() public allowDuplicate: boolean = false;
  @Input() public apiSearch: boolean = false;
  @Input() public removeAllItem: boolean = false;
  @Input() public showList: boolean = true;
  @Input() public forceSearch: number = 0;
  @Input() public caseSensetive: boolean = false;
  @Input() public matchFirst: boolean = false;
  @Input() public custom_item: boolean = true;
  @HostBinding("class.inputref-focused")
  public inputRefFocused: boolean = false;
  @HostBinding("class.input-focused") public inputFocused: boolean = false;
  @HostBinding("class.input-disabled") public inputDisabled: boolean = false;
  public _blurTimeout: any;
  @Output() public changed: EventEmitter<object[]> = new EventEmitter<
    object[]
  >();
  @Output() public added: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() public removed: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() public query: EventEmitter<string> = new EventEmitter<string>();
  public inputValue: string = "";
  public items: Item[] = [];
  public filteredList: Item[] = [];
  public selectedItemIndex: number;
  @ViewChild("selectInputRef", { static: false })
  private _inputElement: ElementRef;

  // for baseInfo types in dynamicReport
  @Input() public baseInfo: boolean = false;
  public pattern: RegExp = / +/g;

  constructor() {}

  @Input() set defaults(defaults: any[]) {
    if (defaults !== undefined) {
      if (!defaults.length) {
        this.items = [];
        this.inputFocused = false;
        return;
      }
      let newItemsValue: object[] = [];
      defaults.forEach((item: { [name: string]: string }) => {
        let newItem: Item = {
          name: item[this.itemkey],
          id: item[this.itemvalue],
        };
        this.items.push(newItem);
        newItemsValue.push(newItem);
        this.added.emit(newItem);
      });
      if (newItemsValue.length) this.changed.emit(newItemsValue);
    }
  }

  public focused(event): void {
    this.inputValue = "";
    this.inputRefFocused = true;
    this.inputFocused = true;
  }

  public blured(event): void {
    if (this.custom_item) {
      let newItem = { name: event.target.value, id: "" };

      this.inputRefFocused = false;
      this.selectedItemIndex = null;

      if (this._blurTimeout) clearTimeout(this._blurTimeout);

      this._blurTimeout = setTimeout(() => {
        if (!this.inputRefFocused) {
          this.inputFocused = false;
        }
        if (!this.inputFocused && !this.inputRefFocused) {
          if (!this.allowDuplicate && this.isExist(newItem)) {
            return;
          }
          if (newItem.name != "" && this.allowAddToInput(newItem)) {
            this.items.push(newItem);
            this.added.emit(newItem);
            this.changed.emit(this.items);
            this._limitCheck();
          }
        }
      }, 200);
    } else {
      this.inputRefFocused = false;
      this.selectedItemIndex = null;

      if (this._blurTimeout) clearTimeout(this._blurTimeout);

      this._blurTimeout = setTimeout(() => {
        if (!this.inputRefFocused) {
          this.inputFocused = false;
          this.inputValue = "";
        }
      }, 200);
    }
  }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    this._filterList();
    this._limitCheck();
    if (this.removeAllItem) {
      this.removaAllItems();
    }
  }

  public keyUp(event): void {
    this.inputFocused = true;
    switch (event.keyCode) {
      case 38: // Up
        if (this.filteredList.length)
          this.selectedItemIndex = !this.selectedItemIndex
            ? this.filteredList.length - 1
            : this.selectedItemIndex - 1;
        return;
      case 40: // Down
        if (this.filteredList.length)
          this.selectedItemIndex =
            this.selectedItemIndex == null ||
            this.selectedItemIndex == this.filteredList.length - 1
              ? 0
              : this.selectedItemIndex + 1;
        return;
      case 13: // Enter
        if (this.selectedItemIndex != null) {
          this.addItem(this.filteredList[this.selectedItemIndex]);
        } else {
          if (this.custom_item) {
            this.addItem({ name: event.target.value, id: "" });
          }
        }
        return;

      case 32: // Space
        break;

      case 27: // Escape
        this.inputFocused = false;
        this.selectedItemIndex = null;
        break;

      default:
        // this.inputChanged();
        break;
    }
  }

  public keyDown(event): void {
    switch (event.keyCode) {
      // case 8: // Backspace
      //   this._handleBackspace();
      //   break;
      // case 46: // Delete
      //   this._handleBackspace();
      //   break;
      case 32: // Space
        if (this.selectedItemIndex != null) {
          this.addItem(this.filteredList[this.selectedItemIndex]);
          event.preventDefault();
        }
        // this.inputChanged();
        else break;
    }
  }

  public keyPress(event): void {}

  public inputChanged(event?): void {
    if (!this.apiSearch) {
      this.query.emit(this.inputValue);
    } else this._filterList();
  }

  public addItem(newItem: Item): void {
    this._focus();
    if (!this.allowDuplicate && this.isExist(newItem)) {
      if (this.items == undefined || this.items == null) return;
      let itemIndex: number = this.items
        .map((item: Item) => (item.name + item.id).toString())
        .indexOf((newItem.name + newItem.id).toString());
      this.removeItem(itemIndex);
      return;
    }
    this.inputValue = "";
    if (this.allowAddToInput(newItem)) {
      this.items.push(newItem);
    }
    this.added.emit(newItem);
    this.changed.emit(this.items);
    this._limitCheck();
  }

  public allowAddToInput(newItem: Item): boolean {
    if (this.list == null || this.list == undefined) return;
    return this.list
      .map((item: Item) => (item.name + item.id).toString())
      .includes((newItem.name + newItem.id).toString());
  }

  public isExist(newItem: Item): boolean {
    if (this.items == null || this.items == undefined) return;
    return this.items
      .map((item: Item) => (item.name + item.id).toString())
      .includes((newItem.name + newItem.id).toString());
  }

  public removeItem(itemIndex: number): void {
    this.removed.emit(this.items[itemIndex]);
    this.items.splice(itemIndex, 1);
    this.changed.emit(this.items);
    this._limitCheck();
  }

  public removaAllItems(): void {
    for (const item of this.items) this.removed.emit(item);
    this.items = [];
    this.changed.emit([]);
    this._limitCheck();
  }

  @HostListener("click")
  private _focus(): void {
    this._inputElement.nativeElement.focus();
  }

  private _limitCheck(): void {
    this.inputDisabled =
      this.disabled ||
      (!this.multi && this.items.length >= 1) ||
      (this.maxLength && this.items.length >= this.maxLength);
  }

  private _filterList(): void {
    if (this.list == null || this.list == undefined) return;
    this.filteredList = this.list
      .map(
        (item: any): Item => {
          return {
            name: item[this.itemkey],
            id: item[this.itemvalue],
          };
        }
      )
      .filter((item: Item) => {
        if (!this.inputValue.length) return true;
        let text = this.caseSensetive ? item.name : item.name.toLowerCase(),
          phrase = this.caseSensetive
            ? this.inputValue
            : this.inputValue.toLowerCase();
        if (this.matchFirst) return text.substring(0, phrase.length) === phrase;
        else return text.includes(phrase);
      });
  }

  private _handleBackspace(): void {
    if (!this.inputValue.length && this.items.length) {
      this.removed.emit(this.items[this.items.length - 1]);
      this.items.length = this.items.length - 1;
      this.changed.emit(this.items);
      this._limitCheck();
    }
  }
}
