import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {UserService} from '../../../services/user.service';

@Component({
  selector: 'auth-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {

  public form: FormGroup;
  public passInputType: "password" | "text" = "password";
  public formSubmitted: boolean = false;

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService
  ) {
    this._migrateForm();
  }

  ngOnInit() {
  }

  public togglePasswordVisibility(): void {
    this.passInputType = this.passInputType == "password" ? "text" : "password";
  }

  public submit(form: FormGroup) {
    this.formSubmitted = true;
    if (form.invalid) return;
    this._userService.register(form.value);
  }

  ngOnDestroy() {
  }

  private _migrateForm(): void {
    this.form = this._fb.group({
      "fullname": ["", Validators.required],
      "cellphone": ["", Validators.compose([Validators.required])],
      "password": ["", Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])],
      // "confirm_password": [""]
    }, {
      // validator: CustomValidators.matchPasswords
    });
  }
}
