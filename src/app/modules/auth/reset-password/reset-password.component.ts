import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {CustomValidators} from "../../../validators/custom-validators";

import {TranslateService} from "@ngx-translate/core";
import {NotificationsService} from "angular2-notifications";

import {ApiService} from "../../../services/api.service";
import {GeneralVariable} from "../../../../general-variable";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styles: [],
})
export class ResetPasswordComponent implements OnInit {
  public userID;
  public title = "";
  public subTitle = "user.username";
  public form: FormGroup;
  public passInputType: "password" | "text" = "password";
  public formSubmitted: boolean = false;
  public errorMessage = "";
  public oldPassError = "";
  private _submitUrl = "user/change-password";

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _route: ActivatedRoute,
    private _translate: TranslateService,
    private _notify: NotificationsService,
    public generalVariable: GeneralVariable,
    private _api: ApiService
  ) {
    this._route.queryParams.subscribe((q) => {
      this.userID = q.id;
    });
    if (this.userID) {
      this._submitUrl = `user/${this.userID}/reset-password`;
      this._getUser();
    }
    this._migrateForm();
  }

  ngOnInit() {
  }

  public togglePasswordVisibility(): void {
    this.passInputType = this.passInputType == "password" ? "text" : "password";
  }

  public submit(form: FormGroup): void {
    this.errorMessage = "";
    this.oldPassError = "";

    this.formSubmitted = true;
    if (form.invalid) return;
    this._api.set(
      this._submitUrl,
      "PUT",
      {
        body: form.value,
      },
      (res: any): void => {
        this._translate.get("notify.reset-pass-success").subscribe((text) => {
          this._notify.success(text.title, text.message);
        });
        if (this.userID) {
          this._router.navigate(["/management/user"]);
        } else {
          this._router.navigate(["/main"]);
        }
      },
      (error) => {
        let errMessage = error.error.error;
        switch (errMessage) {
          case "password length is less than 8":
            this.errorMessage = "forms.feedback.len-password";
            break;
          case "password must contains uppercase character":
            this.errorMessage = "forms.feedback.up-password";

            break;

          case "password must contains lowercase character":
            this.errorMessage = "forms.feedback.low-password";
            break;

          case "password must contains number character":
            this.errorMessage = "forms.feedback.num-password";
            break;
        }
        if (error.error.error == "old password is invalid") {
          this.oldPassError = "forms.feedback.old-pass";
        }
      }
    );
  }

  ngOnDestroy() {
    this._api.remove("auth/change-password");
  }

  private _getUser(): void {
    this._api.set(`user/${this.userID}`, "GET", {}, (res) => {
      this.title = res.user.name;
    });
  }

  private _migrateForm(): void {
    this.form = this._fb.group(
      {
        old_password: this.userID
          ? ""
          : ["", Validators.compose([Validators.required])],
        password: [
          "",
          Validators.compose([Validators.required, Validators.minLength(9)]),
        ],
        confirm_password: "",
      },
      {
        validator: CustomValidators.matchPasswords,
      }
    );
  }
}
