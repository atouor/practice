import {Component, Input, OnInit} from "@angular/core";

import {UserService} from "../../services/user.service";
import {GeneralVariable} from "../../../general-variable";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: [],
})
export class SidebarComponent implements OnInit {
  @Input() public navHeight: number = 0;
  public activeGroup: string = "root";
  public isSuperAdmin = false;
  public sidebarVisibility: boolean = false;
  currentLang: string;

  constructor(
    public userService: UserService,
    public generalVariable: GeneralVariable,
    private _translate: TranslateService,
  ) {
    this.userService.userStorage().user.super_user
      ? (this.isSuperAdmin = true)
      : (this.isSuperAdmin = false);
  }

  public toggleGroup(group: string): void {
    this.activeGroup = this.activeGroup == group ? "root" : group;
  }

  ngOnInit() {
    this.currentLang = this._translate.currentLang;
    this._translate.onLangChange.subscribe((res) => {
      this.currentLang = res["lang"];
    });
  }

  public checkAccess(page, action: string, existItem?: string[]): boolean {
    if (existItem) {
      let exist = existItem.filter((p) => {
        if (this.userService.userAccessible(p, "read")) return p;
      });
      return !!exist.length;
    }
    return this.userService.userAccessible(page, action);
  }

  public logout(event): void {
    event.preventDefault();
    this.userService.logout();
  }

  public sidebarToggle(isNavTitle?: boolean) {
    if (isNavTitle && !this.sidebarVisibility) {
      this.sidebarVisibility = !this.sidebarVisibility;
      document
        .getElementById("sidebar-container")
        .classList.add("open-sidebar");
    } else if (isNavTitle && this.sidebarVisibility) {
    } else {
      this.sidebarVisibility = !this.sidebarVisibility;
      if (this.sidebarVisibility) {
        document
          .getElementById("sidebar-container")
          .classList.add("open-sidebar");
      } else {
        document
          .getElementById("sidebar-container")
          .classList.remove("open-sidebar");
      }
    }
  }
}
