import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class carServices{
    constructor(){}
    // setting defuld value in our data
    car = [
        {id:1 , name:'pride' , year: 1378 ,price: 15000000 , color: 1},
        {id:2 , name:'L90' , year: 1368 ,price: 12000000 , color: 2},
        {id:3 , name:'206SD' , year: 1400 ,price: 5000000 , color: 3},
        {id:4 , name:'benz s500' , year: 1390 ,price: 4750000 , color: 1}
    ]
    // send our data
    getdata(){
        return this.car;
    }
    // my couter for adding new item
    sumID:number ;
    setdata(name:string , year:number , price:number , color:number){
        this.sumID=this.car.length;
        this.sumID++;
     this.car.push( {id:this.sumID , name:name , year:year , price:price , color:color} );
    }
    // for = changing id beacuse we delete an item from data 
    deletedata(id:number){
        for(var i=id; i < this.car.length;i++ ){
            this.car[i].id= this.car[i].id - 1;
        }
        // delete our id resive
        this.car.splice(id-1,1);
        this.sumID=this.car.length+1;
    }
    // editing our data
    editdata(name:string , year:number , price:number , color:number , id:number){
        this.car[id-1].color=color;
        this.car[id-1].id=id;
        this.car[id-1].name=name;
        this.car[id-1].price=price;
        this.car[id-1].year=year;
    }
}