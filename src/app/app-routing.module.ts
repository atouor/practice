
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "./guards/auth.guard";
const routes: Routes = [
  // Dashboard
  {
    path: "dashboard",
    loadChildren: () =>
      import("app/modules/dashboard/dashboard.module").then(
        (m) => m.DashboardModule
      ),
    canActivate: [AuthGuard],
  },
  // cars list
  {
    path: "car",
    loadChildren: () =>
      import("app/modules/cars/cars.module").then(
        (m) => m.Carmoduls
      ),
  },
  // Auth
  {
    path: "auth",
    loadChildren: () =>
      import("app/modules/auth/auth.module").then(
        (m) => m.AuthModule),
  },
  // Other
  {
    path: "",
    redirectTo: "/dashboard/main",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/dashboard/main",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
