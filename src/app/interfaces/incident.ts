namespace ApiModel {
  export interface Incident {
    id: string,
    issue_id: string,
    status: string,
    title: string,
    summery: string,
    description: string,
    reporter: string,
    priority: number,
    ref: string,
    ip: string,
    issue_type: string,
    updated: string,
    asignee: string,
    labels: Array<any>,
    node: string,
    customer: string,
    meta: any,
  }

}
