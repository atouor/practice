namespace ApiModel {
  export interface NodeGroup {
    id: string,
    name: string,
    parent: string,
    children: Array<NodeGroup>
  }
}
