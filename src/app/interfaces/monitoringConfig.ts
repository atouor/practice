namespace ApiModel {

    export interface SNMP {
        enable: boolean
        workers: number;
        intervals: interval;
    }
    export interface ICMP {
        enable: boolean
        workers: number;
        packet_count: number;
        max_rtt: string;
        packet_lost_change_status_to_offline: number;
        intervals: interval;
    }

    export interface interval {
        online: string;
        offline: string;
        warning: string;
    }
}