namespace ApiModel {
  export interface Nodes {
    id: string;
    node_code: string;
    name: string;
    device: IDName;
    group: IDName;
    apn: IDName;
    cpe_model: string;
    node_sla: IDName;
    service_model: Model;
    subscription_status: string;
    bandwidth: Model;
    ip: string;
    wanip: string;
    lanip: string;
    sla: Model;
    rat: string;
    imei: string;
    imsi: string;
    msisdn: string;
    cell_id: string;
    offline_time: any;
    customer: string;
    customer_name: string;
    status: string;
    meta: ApiModel.Meta;
    last_connection_time: string;
    location: Location;
    services: Service[];
    sim_serial_number: string;
    checked: boolean;
    enable: boolean;
    code: boolean;
    frame_route: boolean;
    cpe: Cpe;
    type: string;
    installation_date: string;
    reinstallation_date: string;
    collect_date: string;
    description: string;
    node_type: NodeType;
    icmp: ICMP;
    dynamic_ip: boolean;
  }

  export interface NodeType {
    name: string;
    ID: string;
  }

  export interface Cpe {
    model: Model;
    serial: string;
    mac: string;
  }

  export interface Model {
    name: "";
    id: "";
  }

  export interface Service {
    description: string;
    id: string;
    name: string;
    sensors: Sensor[];
  }

  export interface Sensor {
    created: string;
    icmp: string;
    id: string;
    kpi: string;
    last_time_connected: string;
    name: string;
    oid: string;
    snmp: string;
    ssh: string;
    status: string;
    telnet: string;
    type: string;
    value_unit: string;
  }

  // export interface Meta {
  //   node_code: string,
  //   contact_phone: string,
  //   contact_email: string,
  //   contact_fullname: string,
  //   address: string,
  //   rat_type: IDName,
  //   qci: string,
  //   lac: string,
  //   cell_id: string,
  //   description: string,
  //   installation_date: string
  // }

  export interface Location {
    address: string;
    city: IDName;
    province: IDName;
    latitude: string;
    longitude: string;
  }

  export interface IDName {
    id: string;
    name: string;
  }
}
