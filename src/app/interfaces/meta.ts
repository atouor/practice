namespace ApiModel {
  export interface Meta {
    request_type: string;
    node_name: string;
    node_ip: string;
    node_name_previous: string;
    node_address: string;
    node_address_previous: string;
    node_new_name: string;
    node_new_address: string;
    dedicated_bandwidth: number;
    bandwidth_previous: string;
    bandwidth_new: string;
    lan_address_previous: string;
    lan_address_new: string;
    lat: string;
    msisdn: string,
    long: string;
    address: string;
    contact_per: string;
    contact_phone: string;
    customer_id: string;
    customer_name: string;
    node_id: string;
    contact_fullname: string;
    contact_email: string;
    lac: string;
    lan_ip: string,
    installation_date: string;
    description: string;
    rat_type: string;
    qci: string;
    node_code: string;
  }
}
