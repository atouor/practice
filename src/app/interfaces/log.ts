namespace ApiModel {
  export interface Log {
    id: string;
    client_ip: string;
    created: string;
    method: boolean;
    path: boolean;
    request_body: string[];
    message: string;
    user: string[];
    action: string;
    entity_id: string;
    entity_name: string;
  }
}
