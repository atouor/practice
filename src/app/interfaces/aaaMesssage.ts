namespace ApiModel {
  export interface AAAmessage {
    apn: string;
    connect: boolean;
    created: string;
    duration: string;
    ggsnip: string;
    imei: string;
    imsi: string;
    ip: string;
    last_connection_time: string;
    location: string;
    msisdn: string;
    qci: string;
    ratt: string;
    username: string;
  }
}
