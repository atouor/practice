import { Injectable } from "@angular/core";

@Injectable()
export class GeneralVariable {
  features = {
    user_management: "user_management",
    customer_management: "csutomer_management",
    group_management: "group_management",
    role_management: "role_management",
    apn_management: "apn_management",
    sla_management: "sla_management",
    branch_management: "branch_management",
    sensor_management: "sensor_management",
    kpi_management: "kpi_management",
    map_view: "map_view",
    node_management: "node_management",
    acs_template_management: "acs_template_management",
    update_cpe_management: "update_cpe_management",
    ticketing: "ticketing",
    monitoring_feature: "monitoring_feature",
    direct_notif_feature: "direct_notif_feature",
    event_management: "event_management",
    acs_management: "acs_management",
    task_status: "task_status",
    ping: "ping",
    traceroute: "traceroute",
    report_feature: "report_feature",
    base_info_management: "base_info_management",
    monitoring: "monitoring",
    direct_notification: "direct_notification",
    requesting: "requesting",
    data_management: "data_management",
    aaa_messages: "aaa_messages",
    bulk_operation: "bulk_operation",
    aduit_logs: "aduit_logs",
  };
  serviceType = [{ name: "VPN" }, { name: "VPN+" }];
  ratType = [
    { id: "0", name: `reserved` },
    { id: "1", name: "UTRAN" },
    { id: "2", name: "GERAN" },
    { id: "3", name: "WLAN" },
    { id: "4", name: "GAN" },
    { id: "5", name: "HSPA Evolution" },
    { id: "6", name: "EUTRAN" },
    { id: "7", name: "Virtual" },
    { id: "8", name: "EUTRAN-NB-IoT" },
    { id: "9", name: "LTE-M" },
    { id: "10", name: "NR" },
    { id: "11", name: `spare` },
  ];

  ticket_request_type =
    localStorage.getItem("backpanel.ticket-request-type") &&
    JSON.parse(localStorage.getItem("backpanel.ticket-request-type"));
}
